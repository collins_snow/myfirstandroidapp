package com.example.carselect;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void goToCar(View view){
        Intent intent = new Intent(MainActivity.this, CarActivity.class);
        startActivity(intent);
        finish();
    }

    public void goToAnimation(View view){
        Intent intent = new Intent(MainActivity.this, AnimationActivity.class);
        startActivity(intent);
        finish();
    }

    public void goToForest(View view){
        Intent intent = new Intent(MainActivity.this, ForestActivity.class);
        startActivity(intent);
        finish();
    }
}
