package com.example.carselect;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class CarActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car);
    }

    public void returnToMain(View view){
        Intent intent = new Intent(CarActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
