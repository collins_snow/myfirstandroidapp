package com.example.carselect;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class ForestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forest);
    }

    public void returnToMain(View view){
        Intent intent = new Intent(ForestActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
